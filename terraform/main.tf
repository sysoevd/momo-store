module "yandex_cloud_network" {
    source = "./modules/tf-yc-network"
}
module "yandex_cloud_instance" {
    count = 3
    source = "./modules/tf-yc-instance"
    disk_image_id = "fd839i1233e8krfrf92s"
    net_subnet_id = module.yandex_cloud_network.yandex_vpc_subnets[module.yandex_cloud_network.network_zone[0]].subnet_id
    name = "k8s-node-${count.index}" 
    zone = module.yandex_cloud_network.network_zone[0]
}
module "yandex_cloud_target" {
    source = "./modules/tf-yc-targets"
    nodes = module.yandex_cloud_instance
    net_subnet_id = module.yandex_cloud_network.yandex_vpc_subnets[module.yandex_cloud_network.network_zone[0]].subnet_id
    targets_name = "nodes" 
}
module "yandex_cloud_lb" {
    source = "./modules/tf-yc-lb"
    target_id = module.yandex_cloud_target.id_targets
}