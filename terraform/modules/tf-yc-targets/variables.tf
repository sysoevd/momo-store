variable "net_subnet_id" {
  type        = string
  description = "id of subnet"
  default = "e9bpgri0hc2gos281omv"
}
variable "targets_name" {
  type        = string
  description = "name of targets group"
  default = "nodes"
}
variable "nodes" {}