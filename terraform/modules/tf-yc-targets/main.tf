resource "yandex_lb_target_group" "default" {
  name      = var.targets_name
  target {
    subnet_id = var.net_subnet_id
    address   = var.nodes[0].ip_address
  }
  target {
    subnet_id = var.net_subnet_id
    address   = var.nodes[1].ip_address
  }
  target {
    subnet_id = var.net_subnet_id
    address   = var.nodes[2].ip_address
  }
}