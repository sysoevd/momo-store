resource "yandex_compute_instance" "vm-1" {
  name = var.name
  platform_id               = var.platform_id
  zone                      = var.zone
  # Конфигурация ресурсов:
  # количество процессоров и оперативной памяти
  resources {
    cores  = var.resources_cores
    memory = var.resources_memory
  }
 
  # Загрузочный диск:
  # здесь указывается образ операционной системы
  # для новой виртуальной машины
  boot_disk {
    initialize_params {
      image_id = var.disk_image_id
      size = var.disk_size
    }
  }

  # Сетевой интерфейс:
  # нужно указать идентификатор подсети, к которой будет подключена ВМ
  network_interface {
    subnet_id = var.net_subnet_id
    nat       = var.net_nat
    nat_ip_address =  var.name == "k8s-node-0" ? "84.201.159.28" : "" 
  }

  # Метаданные машины:
  # здесь можно указать скрипт, который запустится при создании ВМ
  # или список SSH-ключей для доступа на ВМ
  metadata = {
    user-data =  "#cloud-config\nusers:\n  - name: ${var.user-data_user}\n    groups: sudo\n    primary_group: ${var.user-data_group}\n    shell: /bin/bash\n    sudo: ['ALL=(ALL) NOPASSWD:ALL']\n    ssh-authorized-keys:\n      - ${var.user-data_ssh-pub}"
  }
  scheduling_policy {
    preemptible = var.vm_preemptible
  }
}
