variable "disk_image_id" {
  type        = string
  description = "image id of disk to boot"
  default = "fd839i1233e8krfrf92s"
}
variable "disk_size" {
  type        = string
  description = "size of boot disk"
  default = "20"
}
variable "net_subnet_id" {
  type        = string
  description = "id of subnet"
  default = "e9bpgri0hc2gos281omv"
}
variable "net_nat" {
  type        = bool
  description = "enable dynamic public IP"
  default = true
}
variable "resources_cores" {
  type        = number
  description = "cores of VM"
  default = 2
}
variable "resources_memory" {
  type        = number
  description = "RAM of VM"
  default = 2
}
variable "zone" {
  type        = string
  description = "name of zone"
  default = "ru-central1-a"
}
variable "user-data_user" {
  type = string
  default = "ansible"
}
variable "user-data_group" {
  type = string
  default = "ansible"
}
variable "user-data_ssh-pub" {
  type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBYjxObSZC7tDwsicxpL9jPcpjusoLa0HTw5P220q4gOxtfy9BOpXPBoDU9v6H7OLVK0BjCDXykzuUBFbYN9J+pO/bBzpXVZ++742kmMBs1Cy6LtL7Gg9A9WWG9YV1Ux/eIgZ5tUbs1/BVVdW+vIm5+65TviZ7d4GzOp4YrdtsSPnf1kkE/MEmaX3Da1cJXFNxdaZbtPHkaQQNbmnHO/jKbWbJlRqGbapZUctDizkdly8o6jtxQVp2xyWXCBLfwrv1Knj5l2+xCRoGfrul73Ek8Y9tOtrSF7xFDDrv/nM/sZFp2Iow2PfIesRO1FUmzyYEwmdkX+BHmxU9yfFRufCJiRQg+STda6i5IYdVUlsqr8R53u9yy7wKWwEzBFV25g+nBmE0AoM91kBzSQNoEQarYMgyt6H3BGc8YN/roQMppRICsoLxfgmT64fe6KahWtL+EWCVLc40wporTfYhudrW1B87Ftr3XkPhj8cjKdrw8T3ouxPmdDTzDMYMgw+w3x0= student@fhm54ms4jq62du8ok4s9"
}
variable "platform_id" {
  type = string
  default = "standard-v3"
}
variable "name" {
  type = string
  default = "chapter5-lesson2-std-019-064"
}
variable "vm_preemptible" {
  type = bool
  default = false
}
