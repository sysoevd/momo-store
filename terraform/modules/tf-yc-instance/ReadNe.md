
#########################Параметры передоваемые в модуль и значения по умолчанию#########################################

disk_image_id  = "fd8bkgba66kkf9eenpkb" # образ загрузочного диска ВМ
disk_size = "20" # размер системного диска 
net_subnet_id" = "e9b3j9sag8o74g739e6v" # идентификатор подсети.
net_nat = true # добавление к ВМ публичного динамического IP адреса.
resources_cores = 2 # кол-во ядер выделенных для ВМ
resources_memory = 2 # кол-во RAM выделенной для ВМ
zone = "ru-central1-a" # зона
user-data_user = "ansible" # имя пользователя который будет создан в гостевой ОС
user-data_group  = "ansible" # группа в которой будет состоять новый пользователь созданный в гостевой ОС
user-data_ssh-pub = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBYjxObSZC7tDwsicxpL9jPcpjusoLa0HTw5P220q4gOxtfy9BOpXPBoDU9v6H7OLVK0BjCDXykzuUBFbYN9J+pO/bBzpXVZ++742kmMBs1Cy6LtL7Gg9A9WWG9YV1Ux/eIgZ5tUbs1/BVVdW+vIm5+65TviZ7d4GzOp4YrdtsSPnf1kkE/MEmaX3Da1cJXFNxdaZbtPHkaQQNbmnHO/jKbWbJlRqGbapZUctDizkdly8o6jtxQVp2xyWXCBLfwrv1Knj5l2+xCRoGfrul73Ek8Y9tOtrSF7xFDDrv/nM/sZFp2Iow2PfIesRO1FUmzyYEwmdkX+BHmxU9yfFRufCJiRQg+STda6i5IYdVUlsqr8R53u9yy7wKWwEzBFV25g+nBmE0AoM91kBzSQNoEQarYMgyt6H3BGc8YN/roQMppRICsoLxfgmT64fe6KahWtL+EWCVLc40wporTfYhudrW1B87Ftr3XkPhj8cjKdrw8T3ouxPmdDTzDMYMgw+w3x0= student@fhm54ms4jq62du8ok4s9"
platform_id = "standard-v3" # ИД платформы на базе которой будет развернута ВМ.
name = "chapter5-lesson2-std-019-064" # имя ВМ
vm_preemptible = true # прерываемая ВМ

модуль выводит информацию о private и public IP адресах
модуль зависит от tf-yc-network, ожидает информацию о подсетях YC