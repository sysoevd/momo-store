resource "yandex_lb_network_load_balancer" "lb1" {
  name = "lb1"
  deletion_protection = "false"
  listener {
    name        = "backend"
    port        = 8081
    target_port = 30007
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
      address = var.lb_ip
    }
  }
  listener {
    name        = "frontend"
    port        = 80
    target_port = 30008
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
      address = var.lb_ip
    }
  }
  listener {
    name        = "grafana"
    port        = 3000
    target_port = 30010
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
      address = var.lb_ip
    }
  }
  listener {
    name        = "prometheus"
    port        = 3010
    target_port = 30011
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
      address = var.lb_ip
    }
  }
  attached_target_group {
    target_group_id = var.target_id
    healthcheck {
      name                = "http"
      interval            = 2
      timeout             = 1
      unhealthy_threshold = 2
      healthy_threshold   = 2
      http_options {
        port = 30007
        path = "/health"
      }
    }
  }
}
