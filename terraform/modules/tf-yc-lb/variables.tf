variable "target_id" {
  type        = string
  description = "id of target"
}
variable "lb_ip" {
  type        = string
  description = "Public IP of LoadBalancer"
  default = "84.252.130.215"
}