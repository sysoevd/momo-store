output "master_ip_address" {
    value = module.yandex_cloud_instance[0].nat_ip_address
}
output "worker1_ip_address" {
    value = module.yandex_cloud_instance[1].nat_ip_address
}
output "worker2_ip_address" {
    value = module.yandex_cloud_instance[2].nat_ip_address
}
output "lb_ip_address" {
    value = module.yandex_cloud_lb.ip
}
